// spark-shell

object DFFromCSV{
    def main(args: Array[String]): Unit = {
        val df = spark.read.options(Map("delimiter" -> "\t", "header" -> "true")).csv("./data/en.openfoodfacts.org.products.csv")

        // leaving only usefull columns
        val Col = Seq("code", "product_name", "energy_100g", "proteins_100g", "fat_100g", "carbohydrates_100g","sugars_100g", "energy-kcal_100g", "saturated-fat_100g", "salt_100g", "sodium_100g", 
        "fiber_100g", "fruits-vegetables-nuts-estimate-from-ingredients_100g", "nutrition-score-fr_100g")
        val data = df.select(Col.head, Col.tail: _*)
        data.write.options(Map("delimiter" -> "\t", "header" -> "true")).csv("cleaned_data_from_3")
    }
}

DFFromCSV.main(Array("Datamart"))

























//import org.apache.spark.sql.{Row, SparkSession}
//import org.apache.spark.sql.types.{IntegerType, StringType, StructType}
//import org.apache.spark.sql.functions.col
//object DropColumn extends App {

//  val spark:SparkSession = SparkSession.builder()
//    .master("local[5]")
//    .appName("SparkByExamples.com")
//    .getOrCreate()
//
//  val data = Seq(
//    Row("James","","Smith","36636","NewYork",3100),
//    Row("Michael","Rose","","40288","California",4300),
//    Row("Robert","","Williams","42114","Florida",1400),
//    Row("Maria","Anne","Jones","39192","Florida",5500),
//    Row("Jen","Mary","Brown","34561","NewYork",3000)
//  )

//  val schema = new StructType()
//    .add("firstname",StringType)
//    .add("middlename",StringType)
//    .add("lastname",StringType)
//    .add("id",StringType)
//    .add("location",StringType)
//    .add("salary",IntegerType)

//  val df = spark.createDataFrame(
//    spark.sparkContext.parallelize(data),schema)
//  df.printSchema()
//  df.show(false)

//  df.drop(df("firstname"))
//    .printSchema()

//  df.drop(col("firstname"))
//    .printSchema()

//  val df2 = df.drop("firstname")
//  df2.printSchema()

//  df.drop("firstname","middlename","lastname")
//    .printSchema()

//  val cols = Seq("firstname","middlename","lastname")
//  df.drop(cols:_*)
//    .printSchema()
//}
