from pyspark.ml.feature import PCA
from pyspark.ml.linalg import Vectors
import numpy as nm
import warnings
from pyspark.sql.functions import monotonically_increasing_id
import numpy as np
import pandas as pd
import pandas as pd
from pyspark.sql import SparkSession
import glob
from pyspark.ml.clustering import KMeans
from pyspark.ml.evaluation import ClusteringEvaluator
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import StandardScaler
from pyspark.sql.functions import col


import scipy.stats as sst

warnings.filterwarnings("ignore")

class ModelSP():
    def __init__(self):
        self.spark = (SparkSession.builder.appName("Clustering").getOrCreate())

        self.filename = glob.glob("cleaned_data_from_3/*.csv")

        self.dat = (self.spark.read.format("csv").option("header", "true").option("delimiter", "\t").load(self.filename))

        self.id_cols = ['code', 'product_name']
        self.nutrition_cols = ['energy_100g', 'proteins_100g', 'fat_100g', 'carbohydrates_100g', 'sugars_100g',
                          'energy-kcal_100g',
                          'saturated-fat_100g', 'salt_100g', 'sodium_100g', 'fiber_100g',
                          'fruits-vegetables-nuts-estimate-from-ingredients_100g', 'nutrition-score-fr_100g']


    def work(self):
        self.dat = self.dat.dropna()
        self.ft = self.dat

        self.ft = self.ft.select( * (col(c).cast("float").alias(c) for c in self.ft.columns[2:]))


        self.assemble = VectorAssembler(inputCols=self.nutrition_cols, outputCol='ftvect')
        self.a_data = self.assemble.transform(self.ft)



        self.scale = StandardScaler(inputCol='ftvect',outputCol='scaled')
        self.data_scale = self.scale.fit(self.a_data)
        self.data_scale_output = self.data_scale.transform(self.a_data)



        pca = PCA(k=4, inputCol="scaled", outputCol="pcaFeatures")
        pca_model = pca.fit(self.data_scale_output)

        self.pca_features = pca_model.transform(self.data_scale_output).select("pcaFeatures")

        # renaming the column features (needed for kmeans.fit)
        self.pca_features = self.pca_features.selectExpr("pcaFeatures as features")

        KMeans_algo = KMeans(featuresCol='features', k=4, )
        KMeans_fit = KMeans_algo.fit(self.pca_features)

        self.output = KMeans_fit.transform(self.pca_features)

        self.output.show()

    def eval(self):

        evaluator = ClusteringEvaluator(predictionCol='prediction', featuresCol='features',
                                        metricName='silhouette', distanceMeasure='squaredEuclidean')

        score = evaluator.evaluate(self.output)
        print("score is: (best is 1 worst is -1) = " + str(score))

        inf = self.dat.select("code", "product_name")

        inf = inf.withColumn("id_1", monotonically_increasing_id())
        output = self.output.withColumn("id_2", monotonically_increasing_id())

        res = inf.join(output, output.id_2 == inf.id_1).drop('id_1')
        res.show()

        res = res.withColumn('id_2', col('id_2').cast('string'))
        res = res.withColumn('prediction', col('prediction').cast('string'))
        res = res.withColumn('features', col('features').cast('string'))

        res.write.option("header", True).option("delimiter", "\t").csv("data_with_lables")


if __name__ == "__main__":
    DB = ModelSP()
    DB.work()
    DB.eval()
