import redis
import csv
import pickle
import os
import sys


class DbManipulator():
    def __init__(self):
        self.pas = sys.argv[1]
        self.redis_host = 'localhost'
        self.redis_port = 6379
        self.pool = redis.ConnectionPool(host=self.redis_host, port=self.redis_port, db=0, password=self.pas)
        self.r = redis.Redis(connection_pool=self.pool)
        self.pipe = self.r.pipeline()
        self.data = []
        self.result = None

    def load_to_db(self):
        with open('clean_train.csv', encoding='utf-8', newline='') as tsvin:
            tsvin = tsvin.readlines()
            for row in tsvin:
                self.data.append([row])

        with open('clean_train.csv', encoding='utf-8', newline='') as cf:
            cfreader = csv.reader(cf)
            headers = next(cfreader)
            print('headers', headers)
            for row in cfreader:
                cnt = 0
                record = {}
                #print(row)
                #break
                for h, v in zip(headers, row):

                    #print("row", row)
                    #print('headers', headers)
                    #break
                    record[h] = v
                #print('row', row[0])
                #print('recoed', record)
                tmp = pickle.dumps(record)
                cnt = cnt+1
                if cnt % 10000 == 0:
                    #print(tmp)
                    print(row[0])
                self.pipe.set(row[0], tmp)
        self.pipe.execute()


        print(len(self.data))
        print(self.data[0])
        print("STEP 1 COMPLETED")

    def from_db_to_datamart(self):
        import pandas as pd

        self.pool = redis.ConnectionPool(host=self.redis_host, port=self.redis_port, db=0, password=self.pas)
        self.r = redis.Redis(connection_pool=self.pool)
        # pipe = r.pipeline()
        keys = self.r.keys('*')
        print(len(keys))

        data = []
        for i in keys:
            res = self.r.get(i)
            cleanres = pickle.loads(res)
            data.append(cleanres)
        pd.DataFrame(data).to_csv('out_from_2.csv', index=False)

        self.result = self.r.get('77')
        print('clean res', self.result)

        cleanres = pickle.loads(self.result)
        print("cleanres", cleanres)
        print("STEP 2 COMPLETED")


if __name__ == "__main__":
    DB = DbManipulator()
    DB.load_to_db()
    DB.from_db_to_datamart()
