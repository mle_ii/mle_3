redis_host = 'localhost'
redis_port=6379
import glob
import redis
import csv
import pickle
import os
import sys



class FinalDb():
    def __init__(self):
        self.pas = sys.argv[1]

        self.pool = redis.ConnectionPool(host=self.redis_host, port=self.redis_port, db=1, password=self.pas)
        self.r = redis.Redis(connection_pool=self.pool)
        self.pipe = self.r.pipeline()

        self.data = []

        self.filenames = glob.glob("data_with_lables/*.csv")

        self.cnt = 0
        self.cnt_t = 0
    def load_to_new_db(self):
        for file in self.filenames:
            with open(file, encoding='utf-8', newline='') as cf:
                cfreader = csv.reader(cf, delimiter='\t', quoting=csv.QUOTE_NONE)
                headers = next(cfreader)
                #headers = [''] + headers

                for row in cfreader:
                    row = [cnt] + row
                    cnt = cnt + 1
                    record = {}
                    #print("row", row)
                    for h, v in zip([''] + headers, row):
                        #print('headers', headers)
                        record[h] = v
                    #print('recoed', record)
                    tmp = pickle.dumps(record)
                    cnt_t = cnt_t + 1
                    if cnt_t % 10000 == 0:
                        print('tmp', tmp)
                    #print('ROW', row)
                    self.pipe.set(row[0], tmp)
                    self.data.append([row])

        self.pipe.execute()
        print("STAGE 5 DONE")
    def check(self):

        result = self.r.get('77')
        print('clean res', result)

        cleanres = pickle.loads(result)
        print("cleanres", cleanres)
        #print(len(data))
        #print(data[0])
        print("CHECK_OK")


if __name__ == "__main__":
    DBF = FinalDb()
    DBF.load_to_new_db()
    DBF.check()
